Ejercicio de java
=================

Consigna
--------
Existe un call center donde hay 3 tipos de empleados: operador,
supervisor y director. El proceso de la atención de una llamada
telefónica en primera instancia debe ser atendida por un operador, si
no hay ninguno libre debe ser atendida por un supervisor, y de no
haber tampoco supervisores libres debe ser atendida por un director.

Solucion
--------

### Prioridad de los empleados


Para manejar la prioridad en el que los empleados son tomados por el dispatcher,
se utilizo la clase `java.util.concurrent.PriorityBlockingQueue`.
La clase `Employee` implementa la interfaz Comparable, la cual
permite ordenar a los empleados por el Rol que tengan.

Debido a que se utiliza solamente esta propiedad, el orden 
es inconsistente respecto al `equals` (
Se da el caso que `e1.compareTo(e2) == 0 && e1.equals(e2) == false`
si `e1.getRole().equals(e2.getRole())`
).

Para evitar que `PriorityBlockingQueue` seleccione un empleado al azar cuando
deba elegir entre empleados con el mismo rol, se creó una clase `FIFOEntry`,
la cual habilita a priorizar a los empleados no solo por su Rol, si no tambien
por el orden en el que ingresan, convirtiendo a la cola priorizada en una cola
priorizada por el rol, y en una cola FIFO cuando el rol es el mismo.

La clase `FIFOEntry` fue copiada directamente de la documentación de PriorityBlockingQueue
[https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/PriorityBlockingQueue.html]

### Dispatcher

El dispatcher posee:

 - un ThreadPoolExecutor, configurado para que use un maximo de 10 threads.
 - una PriorityBlockingQueue, encargada de proveer el siguiente empleado a asignar a la siguiente llamada

#### Threads

Lo primero que sucede cuando ejecutamos `dispatchCall(Call call)`, es ingresar a `ThreadPoolExecutor` un
`java.util.concurrent.Runnable`, el cual se ejecutará inmediatamente si hay threads disponibles,
o se encolará en una `LinkedBlockingQueue` hasta que haya un thread disponible.

Si se encoló la llamada, entonces el dispatcher llamará al metodo `call.onWait(WaitReason)` de la llamada, dando
aviso que no hay un thread disponible.

Luego de esto, `dispatchCall(Call)` retorna un `Future`, el cual le servirá al objeto consumidor para conocer cuando
la llamada terminó, ya sea por un error al querer ejecutar la llamada, una exception lanzada por la llamada,
si por algun motivo el thread fue interrumpido, o si finalizó correctamente.

#### Procesamiento de la llamada

El procesamiento de la llamada esta representado por la clase `Dispatcher.Processor`, un Runnable que
conoce tanto a la llamada como al dispatcher.

Lo primero que hace es sacar un empleado de la cola de empleados, y se lo asigna a la llamada.
Para obtener el empleado, se evitó usar un metodo bloqueante como `queue.take()`. En cambio, 
se utilizó `queue.poll(long, TimeUnit)`. Esto nos permite avisar a la llamada si no hay empleados
disponibles, ejecutando `call.onWait(WaitReason)`, el mismo metodo utilizado cuando no hay
threads disponibles. 

Una vez que se pudo obtener un empleado, ejecuta `call.process()`. 
En base a la consigna establecida, este metodo lo unico que hace es esperar un intervalo 
de tiempo haciendo `Thread.sleep(time)`.

Terminada la llamada, se coloca de nuevo el empleado en la cola. Esto sucede tanto si la llamada
finaliza correctamente, o si tira alguna excepción.


#### Tests

Los tests apuntan a probar que funcione correctamente la paralelización. Se testea hasta 12 llamadas
simultaneas, chequeando que las llamadas son ejecutadas en "dos tiempos": Primero las primeras 10,
que tienen un tiempo fijo, y luego las 2 restantes.

Para testear llamadas concurrentes, se usa un parallelStream, creado a partir de las llamadas que se
van a asignar al llamar a `dispatchCall`.

Al ejecutarse las llamadas en paralelo, y no asignarles un orden en particular (ya que es el comportamiento
esperado), se analiza las propiedades del conjunto de llamadas, y no cada llamada individualmente.

Por ejemplo, en `testWaitOnLackOfThreads`, el cual ejecuta 12 llamadas concurrentes, el test verifica que
haya 10 llamadas que nunca esperaron, y 2 llamadas que tuvieron que esperar al faltar threads, pero no
verifica que una llamada en particular haya esperado.


## Otras consideraciones

Para las llamadas, se creó una interfaz `Call`, la cual es implementada con `CallWithFixedTime`. Esta llamada
espera un tiempo fijo que se setea en el constructor. Esta clase se utiliza en los tests para
facilitar el testeo del dispatcher.

Para cumplir con el ejercicio, se hizo la clase `CallWithRandomTime`, que hereda de `CallWithFixedTime`.
Esta clase solo se limita a llamar al constructor de `CallWithRandomTime`, pasandole como parametro
un tiempo generado al azar. Esto es probado por el test `testRandomTimeCalls`.

Para manejar las prioridades entre empleados, se decidió hacer una unica clase `Employee`, y el rol
esta establecido por el enum `Role`.

## Diagramas

### Clases

![dispatcher](./docs/class.png)

### Relaciones

![dispatcher](./docs/relationships.png)

### Secuencia: Administracion de las llamadas

![dispatcher](./docs/sequence_1.png)

### Secuencia: Procesamiento de las llamadas

En este diagrama, se omite el uso de `FIFOEntry` por motivos de legibilidad

![dispatcher](./docs/sequence_2.png)