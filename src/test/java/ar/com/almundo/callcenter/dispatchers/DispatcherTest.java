package ar.com.almundo.callcenter.dispatchers;

import ar.com.almundo.callcenter.calls.Call;
import ar.com.almundo.callcenter.calls.CallWithFixedTime;
import ar.com.almundo.callcenter.calls.CallWithRandomTime;
import ar.com.almundo.callcenter.persons.Client;
import ar.com.almundo.callcenter.persons.Employee;
import ar.com.almundo.callcenter.persons.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ar.com.almundo.callcenter.dispatchers.TestUtils.*;
import static ar.com.almundo.callcenter.utils.WaitReason.*;
import static ar.com.almundo.callcenter.utils.WaitReason.WAITING_EXECUTOR;

public class DispatcherTest {

    @Test
    public void oneCallOneEmployee() throws InterruptedException, TimeoutException, ExecutionException {
        List<Call> calls = getCalls(1);
        Dispatcher dispatcher = new Dispatcher();

        getEmployees(1).forEach(dispatcher::addEmployee);

        Future<?> future = dispatcher.dispatchCall(calls.get(0));
        Thread.sleep(CALL_TIME_WITH_OFFSET);

        Assertions.assertEquals(null, future.get(0, TimeUnit.MILLISECONDS), "Not all calls finished correctly");
    }

    @Test
    public void oneCallNotWaitToEnd() throws InterruptedException, TimeoutException, ExecutionException {
        List<Call> calls = getCalls(1);
        Dispatcher dispatcher = new Dispatcher();

        getEmployees(1).forEach(dispatcher::addEmployee);

        Future<?> future = dispatcher.dispatchCall(calls.get(0));
        Assertions.assertThrows(TimeoutException.class, () -> future.get(CALL_TIME / 2, TimeUnit.MILLISECONDS));
    }

    @Test
    public void testNineCallsL() throws InterruptedException, TimeoutException, ExecutionException {
        Dispatcher dispatcher = new Dispatcher();
        ThreadPoolExecutor executor = dispatcher.getExecutor();

        List<Call> calls = getCalls(9);
        getEmployees(9).forEach(dispatcher::addEmployee);


        List<Future<?>> result = executeParallel(calls, dispatcher);

        Assertions.assertEquals(9, executor.getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);
        Assertions.assertEquals(9, executor.getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);
        Assertions.assertEquals(0, executor.getActiveCount());

        result.forEach(TestUtils::futureFinishedOk);
    }

    @Test
    public void testTenCalls() throws InterruptedException, TimeoutException, ExecutionException {
        Dispatcher dispatcher = new Dispatcher();

        List<Call> calls = getCalls(10);
        getEmployees(10).forEach(dispatcher::addEmployee);


        List<Future<?>> result = executeParallel(calls, dispatcher);

        Assertions.assertEquals(10, dispatcher.getExecutor().getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);
        Assertions.assertEquals(10, dispatcher.getExecutor().getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);
        Assertions.assertEquals(0, dispatcher.getExecutor().getActiveCount());

        // Chequeo que no haya roto ninguna computacion
        result.forEach(TestUtils::futureFinishedOk);
    }

    @Test
    public void testTwelveCalls() throws InterruptedException, TimeoutException, ExecutionException {
        List<Call> calls = getCalls(12);
        Dispatcher dispatcher = new Dispatcher();

        getEmployees(12).forEach(dispatcher::addEmployee);

        List<Future<?>> result = executeParallel(calls, dispatcher);

        // First, the dispatcher gets 10 calls at the same time
        Assertions.assertEquals(MAX_THREADS, dispatcher.getExecutor().getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);
        Assertions.assertEquals(MAX_THREADS, dispatcher.getExecutor().getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET / 2);

        // Later, the dispatcher gets the other 2 calls
        Assertions.assertEquals(2, dispatcher.getExecutor().getActiveCount());
        Thread.sleep(CALL_TIME_WITH_OFFSET);
        Assertions.assertEquals(0, dispatcher.getExecutor().getActiveCount());

        result.forEach(TestUtils::futureFinishedOk);

    }

    @Test
    public void testCheckMaxThreads() throws InterruptedException, TimeoutException, ExecutionException {
        List<Call> calls = getCalls(12);
        Dispatcher dispatcher = new Dispatcher();
        getEmployees(12).forEach(dispatcher::addEmployee);


        List<Future<?>> result = executeParallel(calls, dispatcher);

        Thread.sleep(CALL_TIME_WITH_OFFSET * 2);

        Assertions.assertEquals(10, dispatcher.getExecutor().getLargestPoolSize());
        result.forEach(TestUtils::futureFinishedOk);
    }

    @Test
    public void testWaitOnLackOfEmployees() throws InterruptedException {
        List<Call> calls = getCalls(2);

        Dispatcher dispatcher = new Dispatcher();
        getEmployees(1).forEach(dispatcher::addEmployee);

        List<Future<?>> result = executeParallel(calls, dispatcher);

        Thread.sleep(CALL_TIME_WITH_OFFSET * 2);

        // Because we are using parallelStream, we can't know which call is executed first.
        // So we need to check if one call never waits for employees
        // and the other call waits.

        TestUtils.CallStatusAggregated status = countCallStatus(calls);

        Assertions.assertEquals(1, status.neverWaits);
        Assertions.assertEquals(1, status.waitsOnlyByEmployee);
        Assertions.assertEquals(0, status.waitsOnlyByExecutor);
        Assertions.assertEquals(0, status.waitsByBoth);

        result.forEach(TestUtils::futureFinishedOk);
    }

    @Test
    public void testWaitOnLackOfThreads() throws InterruptedException {
        List<Call> calls = getCalls(12);

        Dispatcher dispatcher = new Dispatcher();
        getEmployees(12).forEach(dispatcher::addEmployee);

        List<Future<?>> result = executeParallel(calls, dispatcher);

        Thread.sleep(CALL_TIME_WITH_OFFSET * 2);

        TestUtils.CallStatusAggregated status = countCallStatus(calls);

        Assertions.assertEquals(10, status.neverWaits);
        Assertions.assertEquals(0, status.waitsOnlyByEmployee);
        Assertions.assertEquals(2, status.waitsOnlyByExecutor);
        Assertions.assertEquals(0, status.waitsByBoth);

        result.forEach(TestUtils::futureFinishedOk);
    }

    @Test
    public void testRandomTimeCalls() throws InterruptedException, TimeoutException, ExecutionException {
        List<Call> calls = getRandomCalls(10);
        Dispatcher dispatcher = new Dispatcher();

        Integer MAX_TIME = CallWithRandomTime.MAX_TIME + 100;

        System.out.println(calls.stream().mapToInt(Call::getTime).max());
        getEmployees(10).forEach(dispatcher::addEmployee);

        List<Future<?>> result = executeParallel(calls, dispatcher);

        Thread.sleep(MAX_TIME);

        Assertions.assertEquals(0, dispatcher.getExecutor().getActiveCount());
        result.forEach(TestUtils::futureFinishedOk);
    }

    private List<Future<?>> executeParallel(List<Call> calls, Dispatcher dispatcher) {
        return calls
                .parallelStream()
                .map(dispatcher::dispatchCall)
                .collect(Collectors.toList());
    }




}