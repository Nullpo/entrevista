package ar.com.almundo.callcenter.dispatchers;

import ar.com.almundo.callcenter.calls.Call;
import ar.com.almundo.callcenter.calls.CallWithFixedTime;
import ar.com.almundo.callcenter.calls.CallWithRandomTime;
import ar.com.almundo.callcenter.persons.Client;
import ar.com.almundo.callcenter.persons.Employee;
import ar.com.almundo.callcenter.persons.Role;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ar.com.almundo.callcenter.utils.WaitReason.WAITING_EMPLOYEE;
import static ar.com.almundo.callcenter.utils.WaitReason.WAITING_EXECUTOR;

public class TestUtils {
    public static int CALL_TIME = 5000;
    static int CALL_TIME_WITH_OFFSET = 5100;
    static int MAX_THREADS = 10;

    public static void futureFinishedOk(Future<?> future) {
        try {
            Assertions.assertNull(future.get(0, TimeUnit.MILLISECONDS));
        } catch (Exception e) {
            Assertions.fail("Future doesn't finish correctly");
        }
    }


    public static List<Employee> getAllEmployees() {
        List<Employee> employees = new ArrayList<>();

        employees.add(new Employee("Alice", Role.OPERATOR));
        employees.add(new Employee("Bob", Role.OPERATOR));
        employees.add(new Employee("Carol", Role.OPERATOR));
        employees.add(new Employee("Dave", Role.OPERATOR));
        employees.add(new Employee("Eve", Role.OPERATOR));
        employees.add(new Employee("Frank", Role.OPERATOR));
        employees.add(new Employee("Grace", Role.OPERATOR));
        employees.add(new Employee("Heidi", Role.OPERATOR));
        employees.add(new Employee("Judy", Role.OPERATOR));
        employees.add(new Employee("Mallory", Role.OPERATOR));
        employees.add(new Employee("Olivia", Role.OPERATOR));
        employees.add(new Employee("Peggy", Role.OPERATOR));

        return employees;
    }

    public static List<Client> getAllClients(int count) {
        List<Client> clients = new ArrayList<>();

        for(Integer i = 0; i < count; i++){
            clients.add(new Client(i.toString()));
        }

        return clients;
    }

    public static List<Call> getCalls(Integer count) {
        return getAllClients(count)
                .stream()
                .map(c -> new CallWithFixedTime(c, CALL_TIME))
                .collect(Collectors.toList());
    }

    public static List<Call> getRandomCalls(Integer count){
        return getAllClients(count)
                .stream()
                .map(CallWithRandomTime::new)
                .collect(Collectors.toList());
    }

    public static CallStatusAggregated countCallStatus(List<Call> calls) {
        CallStatusAggregated response = new CallStatusAggregated();

        for (Call call : calls) {
            if (noWait(call)) response.neverWaits++;
            else if (waitedOnlyByEmployee(call)) response.waitsOnlyByEmployee++;
            else if (waitedOnlyByExecutor(call)) response.waitsOnlyByExecutor++;
            else response.waitsByBoth++;
        }

        return response;
    }

    public static Stream<Employee> getEmployees(Integer count) {
        return getAllEmployees().stream().limit(count);
    }

    public static class CallStatusAggregated {
        int waitsOnlyByEmployee = 0;
        int waitsOnlyByExecutor = 0;
        int waitsByBoth = 0;
        int neverWaits = 0;
    }

    private static Boolean noWait(Call call) {
        return !call.hasWaitedBy(WAITING_EMPLOYEE) && !call.hasWaitedBy(WAITING_EXECUTOR);
    }

    private static Boolean waitedOnlyByEmployee(Call call) {
        return call.hasWaitedBy(WAITING_EMPLOYEE) && !call.hasWaitedBy(WAITING_EXECUTOR);
    }

    private static Boolean waitedOnlyByExecutor(Call call) {
        return call.hasWaitedBy(WAITING_EXECUTOR) && !call.hasWaitedBy(WAITING_EMPLOYEE);
    }
}