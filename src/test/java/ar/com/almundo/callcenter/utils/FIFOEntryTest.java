package ar.com.almundo.callcenter.utils;

import ar.com.almundo.callcenter.persons.Employee;
import ar.com.almundo.callcenter.persons.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class FIFOEntryTest {
    @Test
    public void testFIFOEntryWithCalls() throws InterruptedException {
        Employee bob = new Employee("Bob", Role.DIRECTOR);
        Employee alice = new Employee("Alice", Role.OPERATOR);
        Employee carol = new Employee("Carol", Role.SUPERVISOR);

        BlockingQueue<FIFOEntry<Employee>> freeEmployees = new PriorityBlockingQueue<>();

        freeEmployees.add(new FIFOEntry<>(bob));
        freeEmployees.add(new FIFOEntry<>(alice));
        freeEmployees.add(new FIFOEntry<>(carol));

        Employee isAlice = freeEmployees.take().getEntry();
        Employee isCarol = freeEmployees.take().getEntry();
        Employee isBob = freeEmployees.take().getEntry();

        Assertions.assertEquals(alice, isAlice);
        Assertions.assertEquals(carol, isCarol);
        Assertions.assertEquals(bob, isBob);
    }
}
