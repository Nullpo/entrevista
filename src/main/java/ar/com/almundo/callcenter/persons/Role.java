package ar.com.almundo.callcenter.persons;

public enum Role {
    OPERATOR,
    SUPERVISOR,
    DIRECTOR;
}
