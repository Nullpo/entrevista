package ar.com.almundo.callcenter.persons;

public class NullEmployee extends Employee {

    private static final Employee instance = new NullEmployee();

    public static final Employee get(){
        return instance;
    }

    private NullEmployee() {
        super("Empleado nulo", Role.OPERATOR);
    }

    @Override
    public int compareTo(Employee o){
        if(o == this) {
            return 0;
        }

        return -1;
    }

}
