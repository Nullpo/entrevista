package ar.com.almundo.callcenter.persons;


/**
 *
 * Class Employee
 *
 * The natural order of this class is determined by the Role property.
 * Because of that, this class doesn't generate a total order relationship, so
 * the natural ordering is inconsistent.
 * You can see the implications of this in the Comparable docs:
 * https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html
 *
 */
public class Employee implements Comparable<Employee> {

    private final String name;
    private final Role role;

    public Employee(String name, Role role) {
        this.name = name;
        this.role = role;
    }

    public int compareTo(Employee o) {
        return this.getRole().compareTo(o.getRole());
    }

    public String getName() {
        return this.name;
    }

    public Role getRole() {
        return this.role;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
