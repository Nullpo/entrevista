package ar.com.almundo.callcenter.utils;

public enum WaitReason {
    WAITING_EMPLOYEE,
    WAITING_EXECUTOR
}
