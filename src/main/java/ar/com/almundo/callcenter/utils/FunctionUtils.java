package ar.com.almundo.callcenter.utils;

import ar.com.almundo.callcenter.calls.Call;

public class FunctionUtils {

    public static <T> T doUntilValue(WaitReason waitReason, Call call, InterruptableSupplier<T> function)
            throws InterruptedException {
        T response;
        Boolean first = true;
        do {
            response = function.get();
            if (response == null && first) {
                call.onWait(waitReason);
                first = false;
            }
        } while (response == null);

        return response;
    }
}
