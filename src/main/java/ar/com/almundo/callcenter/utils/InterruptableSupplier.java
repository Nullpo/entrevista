package ar.com.almundo.callcenter.utils;

public interface InterruptableSupplier<T> {
    T get() throws InterruptedException;
}

