package ar.com.almundo.callcenter.calls;

import ar.com.almundo.callcenter.persons.Client;

import java.util.Random;

public class CallWithRandomTime extends CallWithFixedTime {

    public static final Integer MIN_TIME = 5000;
    public static final Integer MAX_TIME = 10000;

    private static final Random random = new Random();

    public CallWithRandomTime(Client c) {
        super(c, random.nextInt((MAX_TIME - MIN_TIME) + 1) + MIN_TIME);
    }

}
