package ar.com.almundo.callcenter.calls;

import ar.com.almundo.callcenter.persons.Client;
import ar.com.almundo.callcenter.utils.WaitReason;
import ar.com.almundo.callcenter.persons.Employee;

public interface Call {

    void setClient(Client c);
    Client getClient(Client c);

    void onWait(WaitReason waitReason);

    boolean process();

    void setEmployee(Employee employee);

    boolean hasWaitedBy(WaitReason waitReason);

    Employee getEmployee();

    Integer getTime();

}
