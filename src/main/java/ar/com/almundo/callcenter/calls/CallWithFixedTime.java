package ar.com.almundo.callcenter.calls;

import ar.com.almundo.callcenter.persons.Client;
import ar.com.almundo.callcenter.persons.NullEmployee;
import ar.com.almundo.callcenter.utils.WaitReason;
import ar.com.almundo.callcenter.persons.Employee;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class CallWithFixedTime implements Call {

    private Integer time;
    private Client client;
    private Employee employee = NullEmployee.get();
    private Set<WaitReason> hasWaited = Collections.synchronizedSet(new HashSet<WaitReason>());


    public CallWithFixedTime(Client client, Integer time){
        this.time = time;
        this.client = client;
    }

    @Override
    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public Client getClient(Client client) {
        return this.client;
    }

    @Override
    public void onWait(WaitReason waitReason) {
        this.hasWaited.add(waitReason);
    }

    @Override
    public boolean process() {
        try {
            Thread.sleep(this.time);
        } catch (InterruptedException e) {
            throw new RuntimeException("Process interrupted by another thread", e);
        }
        return true;
    }

    @Override
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean hasWaitedBy(WaitReason waitReason) {
        return this.hasWaited.contains(waitReason);
    }


    @Override
    public Employee getEmployee() {
        return this.employee;
    }

    @Override
    public String toString() {
        return this.client.toString() + " -> " + this.employee.toString();
    }

    @Override
    public Integer getTime() {
        return time;
    }


}
