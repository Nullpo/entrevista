package ar.com.almundo.callcenter.dispatchers;


import ar.com.almundo.callcenter.calls.Call;
import ar.com.almundo.callcenter.persons.Employee;
import ar.com.almundo.callcenter.utils.FIFOEntry;
import ar.com.almundo.callcenter.utils.FunctionUtils;
import ar.com.almundo.callcenter.utils.InterruptableSupplier;
import ar.com.almundo.callcenter.utils.WaitReason;

import java.util.concurrent.*;

public class Dispatcher implements DispatcherInterface{

    private static final Integer TIMEOUT = 10;
    private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MILLISECONDS;
    private static final Integer MAX_CONCURRENT_CALLS = 10;

    private BlockingQueue<FIFOEntry<Employee>> freeEmployees = new PriorityBlockingQueue<>();

    private ThreadPoolExecutor executor =  new ThreadPoolExecutor(MAX_CONCURRENT_CALLS, MAX_CONCURRENT_CALLS,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<>());

    private InterruptableSupplier<FIFOEntry<Employee>> getNextEmployee = () -> this.freeEmployees.poll(TIMEOUT, TIMEOUT_UNIT);

    /**
     * The Processor class represents the process to run
     * in the thread pool.
     */
    private class Processor implements Runnable {
        private final Call call;
        private final Dispatcher dispatcher;

        Processor(Call call, Dispatcher dispatcher) {
            this.call = call;
            this.dispatcher = dispatcher;
        }

        @Override
        public void run() {
            try {
                dispatcher.acquireEmployee(call);
            } catch (InterruptedException e) {
                throw new RuntimeException("There was a problem obtaining the employee. See 'Caused by' for more information", e);
            }

            try {
                call.process();
            } finally {
                dispatcher.releaseEmployee(this.call.getEmployee());
            }
        }
    }

    private Employee acquireEmployee(Call call) throws InterruptedException {
        FIFOEntry<Employee> entry = FunctionUtils.doUntilValue(WaitReason.WAITING_EMPLOYEE, call, this.getNextEmployee);
        Employee employee = entry.getEntry();
        call.setEmployee(employee);
        return employee;
    }

    private void releaseEmployee(Employee employee){
        freeEmployees.add(new FIFOEntry<>(employee));
    }

    /**
     * Dispatchs the call to the thread manager.
     *
     * @param call The call to dispatch
     * @return Future Represents the status of the call
     */
    public Future<?> dispatchCall(Call call){
        Future<?> future = executor.submit(new Processor(call, this));

        if(this.executor.getQueue().contains(future) ){
            call.onWait(WaitReason.WAITING_EXECUTOR);
        }

        return future;
    }

    /**
     * Adds an employee to the Employee queue
     * @param e the employee to add
     */
    public void addEmployee(Employee e){
        releaseEmployee(e);
    }

    /**
     * Gets the Thread executor
     * @return ThreadPoolExecutor
     */
    public ThreadPoolExecutor getExecutor(){
        return this.executor;
    }
}
