package ar.com.almundo.callcenter.dispatchers;

import ar.com.almundo.callcenter.calls.Call;

import java.util.concurrent.Future;

public interface DispatcherInterface {
    Future<?> dispatchCall(Call call) throws InterruptedException;
}
