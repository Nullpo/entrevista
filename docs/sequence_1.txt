":Consumer"->*"c:Call": create(client)
"c:Call"->":Consumer": c: Call
":Consumer"->+":Dispatcher":dispatchCall(Call c)

":Dispatcher"->*"p:Processor": create(c, this)
"p:Processor"->":Dispatcher": "p: Processor"
":Dispatcher"->+":ThreadPoolExecutor":submit(p)
":ThreadPoolExecutor"->":Dispatcher":"f: Future"
":Dispatcher"->":ThreadPoolExecutor":getQueue().contains(f)
":ThreadPoolExecutor"->":Dispatcher":"b: Boolean"
opt b is true
    ":Dispatcher"->"c:Call": onWait(WaitReason.WAITING_EXECUTOR)
end
":Dispatcher"->":Consumer": "f:Future"