"pool:ThreadPoolExecutor"->+"p:Processor": run()
"p:Processor"->+"d:Dispatcher": acquireEmployee(call)
"d:Dispatcher"->"d:Dispatcher": getNextEmployee()

loop response == null
    "d:Dispatcher"->+"queue:BlockingQueue": poll(TIMEOUT)
    "queue:BlockingQueue"->-"d:Dispatcher": employee
    opt response == null && first time in loop
         "d:Dispatcher"->"c:Call": onWait(WaitReason.WAITING_EMPLOYEE)
    end
end

"d:Dispatcher"->+"c:Call": setEmployee(employee)
"c:Call"->-"d:Dispatcher": void
"d:Dispatcher"->"p:Processor": void


"p:Processor"->+"c:Call": process()
"c:Call"->-"p:Processor": void
"p:Processor"->"d:Dispatcher": releaseEmployee(employee)
"d:Dispatcher"->"queue:BlockingQueue": add(employee)
"queue:BlockingQueue"->"d:Dispatcher": void
"d:Dispatcher"->"p:Processor": void
"p:Processor"->-"pool:ThreadPoolExecutor": void